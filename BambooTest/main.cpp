#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h> 

// #include <atlbase.h>
// ATL, convert from LPCSTR to LPCWSTR

#include "foo.h"

// https://msdn.microsoft.com/ko-kr/library/windows/desktop/ms633559(v=vs.85).aspx

LRESULT CALLBACK WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) // develop
	{
	case WM_COMMAND:
		break;

	case WM_DESTROY:
		PostQuitMessage(WM_QUIT);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
	// feature ready
	// feature 2 coding~
	// feature 2 ended ---
	// release bug fixed!!!
}

// https://stackoverflow.com/questions/6626397/error-lnk2019-unresolved-external-symbol-winmain16-referenced-in-function
// Linker problem - subsystem, console config - XXX
// Use Not Set to use WinMain

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	//LPSTR className = TEXT("WINDOW");
	//LPCSTR className = _T("WINDOW");

	// What is StringCbprintf and how's this different than general sprintf?
	// https://stackoverflow.com/questions/13269146/what-is-stringcbprintf-and-hows-this-different-than-general-sprintf
	/*TCHAR g_szAppVersionString[16];
	TCHAR pszFileName[MAX_PATH];

	if (StringCbPrintf(g_szAppVersionString,
		sizeof(g_szAppVersionString), _T("Unknown")) != S_OK)
	{
		return -1;
	}*/

	WNDCLASS window;
	window.cbClsExtra = NULL;
	window.cbWndExtra = NULL;
	window.hbrBackground = (HBRUSH)COLOR_BACKGROUND;
	window.hCursor = LoadCursor(hInstance, IDC_ARROW);
	window.hIcon = NULL;
	window.hInstance = hInstance;
	window.lpfnWndProc = WinProc;
	window.lpszClassName = L"WINDOW"; /* LPCSTR(className);*/
	window.lpszMenuName = NULL;
	window.style = CS_HREDRAW | CS_VREDRAW;

	RegisterClass(&window);

	HWND hwnd = CreateWindow(L"WINDOW", L"BAMBOO - Win32 Window Application", WS_OVERLAPPEDWINDOW | WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, 640, 480, NULL, NULL, hInstance, NULL);

	ShowWindow(hwnd, SW_SHOW);
	UpdateWindow(hwnd);

	// MessageBox(NULL, "HELLO", "Hello World", MB_OK);
	// https://stackoverflow.com/questions/3924926/cannot-convert-parameter-1-from-char-to-lpcwstr
	// 'char' to 'LPCWSTR' -  Multi Byte config
	Foo f(L"HelloWorld!");
	f.printMSG();

	MSG message;

	while (GetMessage(&message, NULL, 0, 0))
	{
		TranslateMessage(&message);
		DispatchMessage(&message);
	}
	
}

// test commit for master branch
// master commit - test Bamboo trigger 0
#pragma once
#include <windows.h>

class Foo {
private:
	LPCWSTR msg;

public:
	Foo(LPCWSTR m)
	{
		this->msg = m;
	}
	void printMSG();

	int getAdd(int x, int y);
	
	int getMul(int x, int y);
};

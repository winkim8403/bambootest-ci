#include <windows.h>

#include "foo.h"

void Foo::printMSG()
{
	MessageBox(NULL, LPCWSTR(this->msg), L"Title", MB_OK);
}

int Foo::getAdd(int x, int y)
{
	return x + y;
}

int Foo::getMul(int x, int y)
{
	return x * y;
}

#include "stdafx.h"
#include "CppUnitTest.h"
#include "../BambooTest/foo.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest0
{		
	TEST_CLASS(UnitTest0)
	{
	public:
		
		TEST_METHOD(TestMethod0)
		{
			Foo test(L"MS Unit Test");

			Assert::AreEqual(7, test.getAdd(3, 4));
			Assert::AreNotEqual(8, test.getMul(2, 4));

			// haha
			// testing VSTest Runner
			Assert::AreEqual(1, 1);
			Assert::AreNotEqual(7, 7);
		}

	};

	TEST_CLASS(UnitTest1)
	{
	public:

		TEST_METHOD(TestMethod1)
		{

			Assert::AreEqual(1, 1);
			//Assert::AreEqual(7, 8);
			Assert::AreNotEqual(7, 8);
		}

	};
}